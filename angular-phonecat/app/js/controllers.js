'use strict';

function PhoneListCtrl($scope, Phone) {
  $scope.phones = Phone.query();
  $scope.orderProp = 'age';
}

PhoneListCtrl.$inject = ['$scope', 'Phone'];


function PhoneDetailCtrl($scope, $routeParams, Phone) {
  $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
    $scope.mainImageUrl = phone.images[0];
  });

  $scope.setImage = function(imageUrl) {
    $scope.mainImageUrl = imageUrl;
  };
}

PhoneDetailCtrl.$inject = ['$scope', '$routeParams', 'Phone'];

// function PhoneListCtrl($scope, $http) {
//   $http.get('phones/phones.json').success(function(data) {
//     $scope.phones = data;

//   });

//   $scope.orderProp = 'age';
// }

// function PhoneDetailCtrl($scope, $routeParams) {
//     $scope.phoneId = $routeParams.phoneId;
// }

// function PhoneDetailCtrl($scope, $routeParams, $http) {
//   $http.get('phones/' + $routeParams.phoneId + '.json').success(function(data) {
//     $scope.phone = data;
//   });
// }

// function PhoneDetailCtrl($scope, $routeParams, $http) {
//   $http.get('phones/' + $routeParams.phoneId + '.json').success(function(data) {
//     $scope.phone = data;
//     $scope.mainImageUrl = data.images[0];
//     $scope.hello = function(name) {
//         alert('Hello ' + (name || 'world') + '!');
//     };
//   });

//   $scope.setImage = function(imageUrl) {
//     $scope.mainImageUrl = imageUrl;
//   };
// }


//PhoneDetailCtrl.$inject = ['$scope', '$routeParams', '$http'];
// PhoneListCtrl.$inject = ['$scope', '$http'];
// PhoneDetailCtrl.$inject = ['$scope', '$routeParams']